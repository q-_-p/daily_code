### 复制函数  
```
const el = document.createElement('textarea');
  el.value = str;
  el.setAttribute('readonly', '');
  el.style.position = 'absolute';
  el.style.left = '-9999px';
  document.body.appendChild(el);
  const selected =
    document.getSelection().rangeCount > 0
      ? document.getSelection().getRangeAt(0)
      : false;
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
  if (selected) {
    document.getSelection().removeAllRanges();
    document.getSelection().addRange(selected);
  }
};
//示例
copyToClipboard('Lorem ipsum'); // 'Lorem ipsum' copied to clipboard.

```
### 节流函数  
```
const throttle = (fn, wait) => {
  let inThrottle, lastFn, lastTime;
  return function() {
    const context = this,
      args = arguments;
    if (!inThrottle) {
      fn.apply(context, args);
      lastTime = Date.now();
      inThrottle = true;
    } else {
      clearTimeout(lastFn);
      lastFn = setTimeout(function() {
        if (Date.now() - lastTime >= wait) {
          fn.apply(context, args);
          lastTime = Date.now();
        }
      }, Math.max(wait - (Date.now() - lastTime), 0));
    }
  };
};
//EXAMPLES
window.addEventListener(
  'resize',
  throttle(function(evt) {
    console.log(window.innerWidth);
    console.log(window.innerHeight);
  }, 250)
); // Will log the window dimensions at most every 250ms
```
### 防抖函数  
```
const debounce = (fn, ms = 0) => {
  let timeoutId;
  return function(...args) {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => fn.apply(this, args), ms);
  };
};
//例子
window.addEventListener(
  'resize',
  debounce(() => {
    console.log(window.innerWidth);
    console.log(window.innerHeight);
  }, 250)
); // Will log the window dimensions at most every 250ms
```
### 引用的其他网站的图片资源显示不出来  
`<meta name="referrer" content="no-referrer"/>`


### js读取文件、图片

#### html

```
<input type="file" onchange="previewFile()"><br>
<img src="" height="200" alt="Image preview...">
```

#### javascript

```
javascript
function previewFile() {
  var preview = document.querySelector('img');
  var file    = document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.addEventListener("load", function () {
    preview.src = reader.result;
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}
```
### 验证整数和小数的正则表达式
验证非0开头的无限位整数和小数。整数支持无限位，小数点前支持无限位，小数点后最多保留两位  
js代码如下：  

```
var reg = /^(([^0][0-9]+|0)\.([0-9]{1,2})$)|^(([^0][0-9]+|0)$)|^(([1-9]+)\.([0-9]{1,2})$)|^(([1-9]+)$)/;
```
单独拆分：  
1. 整数：  

```
/^(([^0][0-9]+|0)$)|^(([1-9]+)$)/ 
```
2. 小数：  

```
/^((([^0][0-9]+|0)\.([0-9]{1,2}))$)|^(([1-9]+)\.([0-9]{1,2})$)/ 
```
根据需求可更改：  
1. 若更改小数点前限制位数，则更改 + （例如：小数点前限制4位——([^0][0-9]\d{0,3}|0)——([1-9]]\d{0,3}) ），整数同理。 
2. 若更改小数点后限制位数，则更改 {1,2} （ 例如:：小数点后最多保留3位——{1, 3}；小数点后必须保留3位——{3} ）。  
补充：  
经校验，此正则无法校验第一位就是 . 的数字，如：.1234 。故在程序校验中，还要用indexOf来判断小数点是否在第一位。 

### Promise和计时器setTimeout哪个先执行  

```
let n = new Promise((resolve,reject)=>{
		console.log(1);
	});
let n1 = setTimeout(()=>{
		console.log(2);
	})
console.log("n",n);
console.log("n1",n1);
```
结果!  
[运行结果](https://images.gitee.com/uploads/images/2019/0930/164644_d371f508_4942039.png "20190930164623.png")  

任务队列可以有多个，promise的任务队列，优先级更高  
### 大数相加，要保证精度使用字符串，传参为字符串  

```
function bigNumberSum(a, b) {
	  // 123456789
	  // 000009876
	  let cur = 0;
	  //用0补齐，使两个数的长度相同
	  while (cur < a.length || cur < b.length) {
	    if (!a[cur]) {
	      a = "0" + a;
	    } else if (!b[cur]) {
	      b = "0" + b;
	    }
	    cur++;
	  }
	  console.log('a',a);
	  console.log('b',b);
	  // 存同一位置两数相加的十位数,即超过10的十位的值
	  let carried = 0;
	  const res = [];
	  for (let i = a.length - 1; i > -1; i--) {
		
		//十位的值和同一位置的数字相加,+加号把字符串强制转化成数字+a[i]
	    const sum = carried + +a[i] + +b[i];
		//满10进1，小于归0 
	    if (sum > 9) {
	      carried = 1;
	    } else {
	      carried = 0;
	    }
		// 和的对应的位置的数字
	    res[i] = sum % 10;
	  }
	  // 最大的位置满10进1
	  if (carried === 1) {
	    res.unshift(1);
	  }
	  return res.join("");
}
```
### 二进制转十进制，十进制转二进制  

```
parseInt(100,2)//4
parseInt(4).toString(2)//'100'
```
### 实现加法（不使用已有四则运算）

```
function twoSum(a, b) {
  if (a === 0) return b;
  if (b === 0) return a;
  const res = a ^ b;

  return twoSum(res, (a & b) << 1);
}
```
### 身份证号码正则  
```
/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
```
### 超出文本两行省略  
```
display: -webkit-box;
-webkit-box-orient: vertical;
-webkit-line-clamp: 2;
overflow: hidden;
```
### 样式书签代码
```
javascript: (function(){ var style = document.querySelector('#_outline_'); if (style) { style.parentNode.removeChild(style); } else { style = document.createElement('style'); style.id = '_outline_'; style.innerHTML = "*{outline: 1px solid red}"; document.body.appendChild(style); } })();
```

### 随机字符串方法
```
function generateRandomAlphaNum(len) {
    var rdmString = "";
    for (; rdmString.length < len; rdmString += Math.random().toString(36).substr(2));
    return rdmString.substr(0, len);
}
```
### 千万不要再控制台运行的代码  
```
ﾟωﾟﾉ= /｀ｍ´）ﾉ ~┻━┻   //*´∇｀*/ ['_']; o=(ﾟｰﾟ)  =_=3; c=(ﾟΘﾟ) =(ﾟｰﾟ)-(ﾟｰﾟ); (ﾟДﾟ) =(ﾟΘﾟ)= (o^_^o)/ (o^_^o);(ﾟДﾟ)={ﾟΘﾟ: '_' ,ﾟωﾟﾉ : ((ﾟωﾟﾉ==3) +'_') [ﾟΘﾟ] ,ﾟｰﾟﾉ :(ﾟωﾟﾉ+ '_')[o^_^o -(ﾟΘﾟ)] ,ﾟДﾟﾉ:((ﾟｰﾟ==3) +'_')[ﾟｰﾟ] }; (ﾟДﾟ) [ﾟΘﾟ] =((ﾟωﾟﾉ==3) +'_') [c^_^o];(ﾟДﾟ) ['c'] = ((ﾟДﾟ)+'_') [ (ﾟｰﾟ)+(ﾟｰﾟ)-(ﾟΘﾟ) ];(ﾟДﾟ) ['o'] = ((ﾟДﾟ)+'_') [ﾟΘﾟ];(ﾟoﾟ)=(ﾟДﾟ) ['c']+(ﾟДﾟ) ['o']+(ﾟωﾟﾉ +'_')[ﾟΘﾟ]+ ((ﾟωﾟﾉ==3) +'_') [ﾟｰﾟ] + ((ﾟДﾟ) +'_') [(ﾟｰﾟ)+(ﾟｰﾟ)]+ ((ﾟｰﾟ==3) +'_') [ﾟΘﾟ]+((ﾟｰﾟ==3) +'_') [(ﾟｰﾟ) - (ﾟΘﾟ)]+(ﾟДﾟ) ['c']+((ﾟДﾟ)+'_') [(ﾟｰﾟ)+(ﾟｰﾟ)]+ (ﾟДﾟ) ['o']+((ﾟｰﾟ==3) +'_') [ﾟΘﾟ];(ﾟДﾟ) ['_'] =(o^_^o) [ﾟoﾟ] [ﾟoﾟ];(ﾟεﾟ)=((ﾟｰﾟ==3) +'_') [ﾟΘﾟ]+ (ﾟДﾟ) .ﾟДﾟﾉ+((ﾟДﾟ)+'_') [(ﾟｰﾟ) + (ﾟｰﾟ)]+((ﾟｰﾟ==3) +'_') [o^_^o -ﾟΘﾟ]+((ﾟｰﾟ==3) +'_') [ﾟΘﾟ]+ (ﾟωﾟﾉ +'_') [ﾟΘﾟ]; (ﾟｰﾟ)+=(ﾟΘﾟ); (ﾟДﾟ)[ﾟεﾟ]='\\'; (ﾟДﾟ).ﾟΘﾟﾉ=(ﾟДﾟ+ ﾟｰﾟ)[o^_^o -(ﾟΘﾟ)];(oﾟｰﾟo)=(ﾟωﾟﾉ +'_')[c^_^o];(ﾟДﾟ) [ﾟoﾟ]='\"';(ﾟДﾟ) ['_'] ( (ﾟДﾟ) ['_'] (ﾟεﾟ+(ﾟДﾟ)[ﾟoﾟ]+ (ﾟДﾟ)[ﾟεﾟ]+(ﾟΘﾟ)+ (ﾟｰﾟ)+ (ﾟΘﾟ)+ (ﾟДﾟ)[ﾟεﾟ]+(ﾟΘﾟ)+ ((ﾟｰﾟ) + (ﾟΘﾟ))+ (ﾟｰﾟ)+ (ﾟДﾟ)[ﾟεﾟ]+(ﾟΘﾟ)+ (ﾟｰﾟ)+ ((ﾟｰﾟ) + (ﾟΘﾟ))+ (ﾟДﾟ)[ﾟεﾟ]+(ﾟΘﾟ)+ ((o^_^o) +(o^_^o))+ ((o^_^o) - (ﾟΘﾟ))+ (ﾟДﾟ)[ﾟεﾟ]+(ﾟΘﾟ)+ ((o^_^o) +(o^_^o))+ (ﾟｰﾟ)+ (ﾟДﾟ)[ﾟεﾟ]+((ﾟｰﾟ) + (ﾟΘﾟ))+ (c^_^o)+ (ﾟДﾟ)[ﾟεﾟ]+(ﾟｰﾟ)+ ((o^_^o) - (ﾟΘﾟ))+ (ﾟДﾟ)[ﾟεﾟ]+(oﾟｰﾟo)+ ((ﾟｰﾟ) + (ﾟΘﾟ))+ (c^_^o)+ (ﾟДﾟ) .ﾟΘﾟﾉ+ (ﾟДﾟ) .ﾟΘﾟﾉ+ (ﾟДﾟ)[ﾟεﾟ]+(oﾟｰﾟo)+ ((ﾟｰﾟ) + (ﾟｰﾟ) + (ﾟΘﾟ))+ (c^_^o)+ (o^_^o)+ (ﾟДﾟ) ['c']+ (ﾟДﾟ)[ﾟεﾟ]+(ﾟｰﾟ)+ ((o^_^o) - (ﾟΘﾟ))+ (ﾟДﾟ)[ﾟεﾟ]+((ﾟｰﾟ) + (ﾟΘﾟ))+ (ﾟΘﾟ)+ (ﾟДﾟ)[ﾟoﾟ]) (ﾟΘﾟ)) ('_');
```