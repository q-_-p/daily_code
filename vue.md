### html2canvas在IOS13.4版本上失效问题  
将版本降低至`"html2canvas": "1.0.0-rc.4"`可解决，如果不能第二步
```
// 使用, 全局window对象加入html2canvas，如果没有window就调用原来的，所以需要做判断
(window.html2canvas || html2canvas)(shareContent, opts).then(canvas => {
  let url = canvas.toDataURL("image/png");
  console.log(url)
}).catch(err => {
  // do sth
});
```
### [IOS微信H5支付无法跳转回APP问题](https://developers.weixin.qq.com/community/develop/doc/00080a6473867079c8e83aa3356404)
```
//app配置支付返回跳转app链接`redirect_url=URLEncode(A.company.com://)`
const iframe = document.createElement('iframe')
      iframe.style.display ='none'
      //url如https://wx.tenpay.com/cgi-bin/mmpayweb-bin/checkmweb?prepay_id=********&package=*****&redirect_url=http%3A%2F%2Fxiaodu.xiachunlong.cn%2Fapi%2Fmoney_charge%2Frecharge_return%3Ftype%3Dwechat%26out_trade_no%3D2*****
      iframe.setAttribute('src', res.data.data.url)
      frame.setAttribute('sandbox','allow-top-navigation allow-scripts')
      document.body.appendChild(iframe)
      setTimeout(()=>{
         document.body.removeChild(iframe)
         this.$dialog.confirm({
             title:'提示',
             message:'请确认微信支付是否完成',
         }).then(()=>{
             //传订单号，校验支付接口
             this.checkPay(res.data.data.order_no);
         }).catch(err=>{
             // 取消支付
         })
       },5000)
```

### 使用HbuilderX打包vue应用  
#### Vue打包配置  
1. 路由模式换成hash  
2. axios请求地址添加生产环境全地址baseURL  
3. vue.config.js打包输出地址`publicPath:'./'`  
#### HbuilderX配置  
1. 新建H5+项目  
2. 将vue打包后的dist内文件放到项目根目录下，将已有的相同目录文件替换  
3. mainfest.json配置->APP常用其他配置->CPU类型全选 
### window 安装 node-sass  
1.先检查项目的node_modules文件夹是否存在node-sass文件夹，存在，删掉，不存在，执行第二步；
2.打开cmd，在项目根目录下输入以下代码  
```
npm install node-sass --sass-binary-site=http://npm.taobao.org/mirrors/node-sass
```
### vue-cli2打包不打包map文件  
#### 只需把config/index.js中的productionSourceMap: true改为productionSourceMap: false即可  
### vue打包优化  
#### https://segmentfault.com/a/1190000012249890
### 解决数据层级太多更不能实时更新视图问题 因为数据层次太多，render函数没有自动更新，需手动强制刷新this.$forceUpdate()
### 如何配置404页面详细
https://juejin.im/post/5b019ad7f265da0ba567d259
### 404页面的设置简单配置
 如果SPA的路由表是固定的，那么配置404页面就变得非常的简单。只需要在路由表中添加一个路径为404的路由，同时在路由表的最底部配置一个路径为*的路由，重定向至404路由即可。
（由于路由表是由上至下匹配的，一定要将任意匹配规则至于最底部，否则至于此路由规则下的路由将全部跳转至404，无法正确匹配。）
#### 
```
// router.js  

export default new Router({  

  mode: 'history',  
  
  routes: [  
  
    // ...  
    
    {  
    
      name: '404',  
      
      path: '/404',  
      
      component: () => import('@/views/notFound.vue')  
      
    },  
    
    {  
    
      path: '*',    // 此处需特别注意至于最底部  
      
      redirect: '/404'  
      
    }  
    
  ],  
  
})  

```
### vue轮播插件vue-awesom-swiper bug
在项目中使用vue-awesome-swiper如果loop和autoplay总是出现各种问题,第一次加载的时候,轮播是不动的,需要重新加载一下swiper才会轮播
//轮播设置
```
swiperOption: {
　　direction: 'vertical',
　　observer:true,//修改swiper自己或子元素时，自动初始化swiper 
　　observeParents:true,//修改swiper的父元素时，自动初始化swiper 
　　loop:true,
　　autoplay: {
　　　　delay: 2000,
　　disableOnInteraction: false
　　}
}
```
需要添加上两个属性,这样达到一个初始化swiper的目的
observer:true,//修改swiper自己或子元素时，自动初始化swiper 
observeParents:true,//修改swiper的父元素时，自动初始化swiper 

### vue中使用swiper-slide时，循环轮播失效？

loop  设置为true 则开启loop模式。loop模式：会在原本slide前后复制若干个slide(默认一个)并在合适

的时候切换，让Swiper看起来是循环的。 
loop模式在与free模式同用时会产生抖动，因为free模式下没有复制slide的时间点。

在原本基础上复制若干个slide，可是在vue的v-for中时，异步加载的数据都还没有返回时，就先加载了Swiper组件并复制了sliper

解决办法：利用v-if的属性，v-if=加载列表.length，确保异步加载的数据已经返回后，再加载swiper组件

### 使用Sass
npm install --save-dev sass-loader //sass-loader依赖于node-sass
npm install --save-dev node-sass
//build/webpack.base.conf/
{
  test:/\.scss/,
  loaders:["style","css","sass"]
},
### 错误配置页
import Error from '@/views/Error.vue'
{
     path: '*',
     component: Error
},

//摘自https://blog.csdn.net/m0_38069630/article/details/79172700

### proxy代理
https://www.jb51.net/article/138539.htm
https://www.cnblogs.com/huoerheaven/p/9701125.html

### 移动端适配方案2
https://www.jianshu.com/p/3f38841515cd
### 输入框正整数包括0
```
<input type="tel"
      v-model="buy_feed_num"
      pattern="[0-9]*"
      oninput = "value=value.replace(/[^\d]/g,'')"
      >
```
### 监听图片加载的插件，包括背景图片
https://github.com/xiaobai2233/imagesloaded
### 禁止用户缩放页面
```
<meta name=viewport content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
```
### v-for 和v-if不能同时使用的解决办法  
原代码
```
 <ul>
<li
  v-for="user in users"
  v-if="user.isActive"
  :key="user.id"
>
  {{ user.name }}
</li>
  </ul>
```  
修改后的代码  
```
computed: {
activeUsers: function () {
return this.users.filter(function (user) {
  return user.isActive
})
}
}
<ul>
<li
  v-for="user in activeUsers"
  :key="user.id"
>
{{ user.name }}
</li>
</ul>
```
### vue 使用input动态保留两位小数  
```
<template>
<input
      v-model="num"
      type="tel"
      placeholder="保留两位小数"
      @input="toFixedtwo($event)"
/>
</template>
methods:{
  toFixedtwo(e){
    // 通过正则过滤小数点后两位
      e.target.value = (e.target.value.match(/^\d*(\.?\d{0,2})/g)[0]) || null
      this.num= e.target.value;
      console.log('e', e.target.value)
  }
}
```
### [Vue插件大全  ](https://blog.csdn.net/qq_25838839/article/details/84613644)  
如环形进度条插件npm install --save vue-radial-progress  
波纹效果插件 npm install vue-touch-ripple --save
### vuecli3生产环境去除console.log()  
\node_modules\@vue\cli-service\lib\config\terserOptions.js  
compress 添加  
```
warnings: false,
drop_console: true,
drop_debugger: true,
pure_funcs: ['console.log']
```
### vuecli3生产环境去除console.log()插件  
npm install babel-plugin-transform-remove-console -D  
修改babel.config.js
```
const plugins = []
if (process.env.NODE_ENV === 'production') {
  plugins.push('transform-remove-console')
}
module.exports = {
  presets: [
    '@vue/app',
  ],
  plugins
}
```
### 手机端键盘将页面弹起，兼容大部分  
```
<template>
    <div ref="content"></div>
</template>
```
```
mounted(){
    this.$refs.content.style.height = window.innerHeight + 'px';
}
```
### 当子组件想要访问父组件甚至祖父组件的数据时候  
provide 选项允许我们指定我们想要提供给后代组件的数据/方法。在这个例子中，就是 <google-map> 内部的 getMap 方法：
```
provide: function () {
  return {
    getMap: this.getMap
  }
}
```
然后在任何后代组件里，我们都可以使用 inject 选项来接收指定的我们想要添加在这个实例上的属性：
```
inject: ['getMap']
```
注意：此接受数据的方法是非响应式的，初始值是什么子组件接收的就是什么  
### 常用代码段——滚动透明度增加  
```
<template>
<div class="wallet" ref="home_dom">
<div class="header" ref="search_dom"></div>
</div>
</template>
mounted(){
      this.$nextTick(()=>{
        this.$refs.home_dom.addEventListener("scroll", (e)=>{
            let scrolltop = e.target.scrollTop;
            let opacity = parseFloat(scrolltop/200) > 1?1:parseFloat(scrolltop/200);
            this.$refs.search_dom.style.background=`rgba(231, 0, 18,${opacity})`;
        });
      })
      
  },
```